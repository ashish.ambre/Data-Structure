/*------------------------------------------------------------------------------------------------------------
 *  Project 	: 	SimpleSingleLinkedList
 *  File    	:	SimpleSingleLinkedList.c
 *  Auther	: 	Ashish Ambre
 *  Reference	:	gitlab.com/CodeKhazana
 *
 * Description 	:
 * 	This code demonstrates the working of simple single linked list
 *-----------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

struct Node
{
	int value;
	struct Node *next;
};

struct Node *head = NULL; 	// points to the first node in the linked list
struct Node *tail = NULL;	// points to the last node in the linked list

int main(void)
{
	int val;

	printf("Enter a value to add to the linked list ( 0 to Stop ): ");
	scanf("%d", &val);

	while(val != 0)
	{
		// allocation of the node on the heap
		struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));

		newNode->next = NULL;
		newNode->value = val;

		// If the linked list is empty, head and tail will be NULL

		if(NULL == head)
		{
			// head is NULL, so first node
			// Since this is the first node, make it head as well as tail
			head = newNode;
			tail = newNode;
		}
		else
		{
			// Since the list is not empty, we will add this node to the end of the list
			tail->next = newNode;
			tail = newNode;
		}
		
		// Ask for more values

		printf("Enter a value to add to the linked list ( 0 to Stop ): ");
		scanf("%d", &val);
	}

	// Print the linked list
	
	// Start with the head node
	struct Node *current = head;

	// If it is not null print it, otherwise exit
	while(current != NULL)
	{
		printf("%d ", current->value);
		current = current->next;
	}
	printf("\n");
	return 0;
}



