/*----------------------------------------------------------------------------------------------------------------
 * Project 	: 	SingleLinkedList
 * File		: 	SingleLinkedList.c
 * Auther 	: 	Ashish Ambre
 *
 * Description:
 * 	This C program demonstrates a Single Linked List with structures and methods
 *
 * Version 	1.0	02-Jun-2018 	Initial release
 * --------------------------------------------------------------------------------------------------------------*/
 
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// Enum for bool as C does not have a bool type
typedef enum
{
	false,
	true
}bool;

typedef struct node 
{
	int value;
	struct node *next;
}Node;

typedef struct list
{
	Node *head;	// Pointer to the first node
	Node *tail;	// Pointer to the last node
}List;

/* 
 * For initialization of the head and tail pointers
 */
void Init(List *pList)
{
	pList->head = NULL;
	pList->tail = NULL;
}

/*
 *  List function to add new node to back
 */
bool addToBack(List *pList, int val)
{
	Node *newNode  = (Node *)calloc(1,sizeof(Node));

	// If for some reason memory allocation fails, return false
	if(NULL == newNode)
		return false;

	newNode->value = val;

	if(!pList->head)	// head is NULL
	{
		pList->head = newNode;
	}
	else
	{
		pList->tail->next = newNode;
	}

	// Finally the tail will point to the recently added node
	pList->tail = newNode;
	return true;
}

/*
 *  List function to add new node to front
 */
bool addToFront(List *pList, int val)
{
	Node *newNode = (Node *)calloc(1,sizeof(Node));

	if(NULL == newNode)
		return false;

	newNode->value = val;

	if(!pList->tail)	// tail is NULL
	{
		pList->tail = newNode;
	}
	else
	{
		newNode->next = pList->head;
	}

	pList->head = newNode;
	return true;
}

/*
 * List function to print the linked list forword 
 */
int printForward(const List *pList)
{
	int count = 0; 	// We'll rerurn the number of nodes printed

	for(Node *current = pList->head; current; current = current->next)
	{
		printf("%d\t", current->value);
		count++;
	}
	printf("\n");
	return count;
}

int main(void)
{
	List mylist;

	Init(&mylist);

	int val;
	char addWhere;

	while(printf("Enter the value ( 0 to stop )"),
		scanf("%d", &val),
		val)
	{
		printf("Do you want to add it to the back or front (B/f): ");

		int clearbuffer = getchar();

		addWhere = getchar();
		printf("%c\n", addWhere);

		if(toupper(addWhere) == 'F')
			addToFront(&mylist, val);
		else
			addToBack(&mylist, val);
	}
	printForward(&mylist);

	return 0;
}
